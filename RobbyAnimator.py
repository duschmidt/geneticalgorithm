from Tkinter import *
import random, time, threading
from Individual import *
class Display(Frame):
	threadOn = False
	h = 0
	w = 0
	map = []
	Surface = Canvas
	root = Tk()
	indiv = Individual
	moves = {'start':[0,0],'moves':""}
	bot = {'id':0,'pos':[0,0],'dest':[0,0]}
	def __init__(self):
		Frame.__init__(self,master=self.root)
		self.pack()
		self.createWidgets()
		self.smart = "254413355252212251453154554351452353350310255451454422455453255453150253154254011455240220452451440454111355450145453453441451354110041123450024102452141411514514153154354151155523154114012352352351152112424023032412311053142424233543241452004" 
		self.medium ="254154250250150250453151243355150255352414203354050251451151403420150354153302141440243353455215323412114405350340221453213442041100331251153423403140504150341150355321235011215234100152505033352002353102414014001533433355343045020001345240510"
		self.dumb ="255155000251152204153153030355354351354121240115354330452402351042142520551320202450440253440230443402454550351242453422223210000031512023450112252024203202034525154326201100215520114353210232354223120153131013130422324522530435344500054014253"
		self.indiv = Individual()
		self.indiv.GeneticCode = self.dumb
		self.indiv.name = "dumb"
	def createWidgets(self):
		self.h = 300
		self.w = 300
		self.Surface = Canvas(self, width=self.w, height = self.h, bg="#FFFFFF")
		self.Surface.bind("<Button-1>", self.toggleThread)
		self.Surface.bind("<Button-3>", self.resetMap)
		self.Surface.bind("<Button-2>", self.toggleBrain)
		self.Surface.pack()
		
		for j in range(10):
			i = j*(self.w/10.0)
			self.Surface.create_line(i,0,i,self.h)
			i = j*(self.h/10.0)
			self.Surface.create_line(0,i,self.w,i)
		self.robby = [random.randint(0,9), random.randint(0,9)]
		pos = self.cellToPix(self.robby)
		self.bot['id'] = self.Surface.create_rectangle(pos[0]-10,pos[1]-10,pos[0]+10,pos[1]+10,fill='#00f')
		self.makeMap()	
	def resetMap(self, evt):
		self.makeMap()
	def makeMap(self):
		if not len(self.map) == 0:
			for i in range(10):
				for j in range(10):
					if self.map[i][j]['id'] >= 0:
						self.Surface.delete(self.map[i][j]['id'])
		self.map = []
		for i in range(10):
			self.map.append([])
			for j in range(10):
				if random.random() > 0.8:
					cell = 'X'
					
					pos = self.cellToPix([i,j])
					id = self.Surface.create_rectangle(pos[0]-5,pos[1]-5,pos[0]+5,pos[1]+5,fill='#f00')
				else:
					cell = 'O'
					id = -1
				self.map[i].append({'val':cell,'id':id})

	def cellToPix(self, cell):
		row = cell[1]
		column = cell[0]
		span = self.w / 10.0
		x = span*(column + .5)
		y = span*(row + .5)
		pix = [x,y]
		return pix
	
	def toggleThread(self, evt):
		if self.threadOn == False:
			self.thrd = threading.Thread(target = self.buildMoves)
			self.threadOn = True
			self.thrd.start()
		else:
			self.threadOn = False
			
	def toggleBrain(self, evt):
		if self.indiv.name == "smart":
			print "set to dumb"
			self.indiv.GeneticCode = self.dumb
			self.indiv.name = "dumb"
		elif self.indiv.name == "medium":
			print "set to smart"
			self.indiv.GeneticCode = self.smart
			self.indiv.name = "smart"
		elif self.indiv.name == "dumb":
			print "set to medium"
			self.indiv.GeneticCode = self.medium
			self.indiv.name = "medium"
	def buildMoves(self):
		robby = self.robby
		RMap = ['stay put','move Up','move Down','move Left','move Right','pickUp','random']
		pos = self.cellToPix(robby)
		self.bot['pos']=pos
		self.Surface.coords(self.bot['id'],self.bot['pos'][0]-10,self.bot['pos'][1]-10,self.bot['pos'][0]+10,self.bot['pos'][1]+10)
		score = 0
		for i in range(200):
			#build scenario vars in order: center, up, down, left, right
			if not self.threadOn:
				break
			scenario = []
			#check center
			if self.map[robby[0]][robby[1]]['val'] == 'O':
				scenario.append(0)
			elif self.map[robby[0]][robby[1]]['val'] == 'X':
				scenario.append(1)
			#check up
			if robby[0] == 0: #in top row, up is wall
				scenario.append(2)
			elif self.map[robby[0]-1][robby[1]]['val'] == 'O':
				scenario.append(0)
			elif self.map[robby[0]-1][robby[1]]['val'] == 'X':
				scenario.append(1)
			#check down
			if robby[0] == 9: #in bottom row, down is wall
				scenario.append(2)
			elif self.map[robby[0]+1][robby[1]]['val'] == 'O':
				scenario.append(0)
			elif self.map[robby[0]+1][robby[1]]['val'] == 'X':
				scenario.append(1)
			#check left
			if robby[1] == 0: #in left column, left is wall
				scenario.append(2)
			elif self.map[robby[0]][robby[1] - 1]['val'] == 'O':
				scenario.append(0)
			elif self.map[robby[0]][robby[1] - 1]['val'] == 'X':
				scenario.append(1)
			#check right
			if robby[1] == 9: #in right column, right is wall
				scenario.append(2)
			elif self.map[robby[0]][robby[1] + 1]['val'] == 'O':
				scenario.append(0)
			elif self.map[robby[0]][robby[1] + 1]['val'] == 'X':
				scenario.append(1)
			
			R = self.indiv.getResponse(scenario)
			if RMap[R] == 'random':
				R = random.randint(1,4)
			if RMap[R] == 'stay put':
				print "score: " + str(score) + " move: " + str(i) + " STAY PUT"
			if RMap[R] == 'move Up':
				if robby[0] == 0:
					score -= 5
					print "score: " + str(score) + " move: " + str(i) + " HIT WALL" 
					#bump top wall
				else:
					robby[0] -= 1
					self.botGoto(robby)
			if RMap[R] == 'move Down':
				if robby[0] == 9:
					score -= 5
					print "score: " + str(score) + " move: " + str(i) + " HIT WALL"
					#bump bottom wall
				else:
					robby[0] += 1
					self.botGoto(robby)
			if RMap[R] == 'move Left':
				if robby[1] == 0:
					score -= 5
					print "score: " + str(score) + " move: " + str(i) + " HIT WALL"
					#bump left wall
				else:
					robby[1] -= 1
					self.botGoto(robby)
			if RMap[R] == 'move Right':
				if robby[1] == 9:
					score -= 5
					print "score: " + str(score) + " move: " + str(i) + " HIT WALL"
					#bump right wall
				else:
					robby[1] += 1
					self.botGoto(robby)
			if RMap[R] == 'pickUp':
				if self.map[robby[0]][robby[1]]['val'] == 'X':
					self.Surface.itemconfigure(self.map[robby[0]][robby[1]]['id'],fill='#fff')
					self.map[robby[0]][robby[1]]['val'] = 'O'
					score += 10
					print "score: " + str(score) + " move: " + str(i) + " PICK UP GOOD!"
				elif self.map[robby[0]][robby[1]]['val'] == 'O':
					score -= 1
					print "score: " + str(score) + " move: " + str(i) + " ERROR PICKUP"
	
	def botGoto(self, cell):
		self.bot['dest'] = self.cellToPix(cell)
		while self.bot['pos'] != self.bot['dest']:
			if self.bot['pos'][0] > self.bot['dest'][0]:
				self.bot['pos'][0] -= 1
			elif self.bot['pos'][0] < self.bot['dest'][0]:
				self.bot['pos'][0] += 1
			if self.bot['pos'][1] > self.bot['dest'][1]:
				self.bot['pos'][1] -= 1
			elif self.bot['pos'][1] < self.bot['dest'][1]:
				self.bot['pos'][1] += 1
			self.Surface.coords(self.bot['id'],self.bot['pos'][0]-10,self.bot['pos'][1]-10,self.bot['pos'][0]+10,self.bot['pos'][1]+10)	
			time.sleep(.01)
	
	def botTravel(self):
		if self.bot['pos'] != self.bot['dest']:
			if self.bot['pos'][0] > self.bot['dest'][0]:
				self.bot['pos'][0] -= 1
			elif self.bot['pos'][0] < self.bot['dest'][0]:
				self.bot['pos'][0] += 1
			if self.bot['pos'][1] > self.bot['dest'][1]:
				self.bot['pos'][1] -= 1
			elif self.bot['pos'][1] < self.bot['dest'][1]:
				self.bot['pos'][1] += 1
			self.Surface.coords(self.bot['id'],self.bot['pos'][0]-10,self.bot['pos'][1]-10,self.bot['pos'][0]+10,self.bot['pos'][1]+10)	
			self.Surface.after(50,self.botTravel)
	
	def buildMoves2(self):
		robby = [random.randint(0,9), random.randint(0,9)]
		self.moves['start']=robby
		self.moves['moves']=""
		RMap = ['stay put','move Up','move Down','move Left','move Right','pickUp','random']
		for i in range(200):
			#build scenario vars in order: center, up, down, left, right
			scenario = []
			#check center
			if self.map[robby[0]][robby[1]] == 'O':
				scenario.append(0)
			elif self.map[robby[0]][robby[1]] == 'X':
				scenario.append(1)
			#check up
			if robby[0] == 0: #in top row, up is wall
				scenario.append(2)
			elif self.map[robby[0]-1][robby[1]] == 'O':
				scenario.append(0)
			elif self.map[robby[0]-1][robby[1]] == 'X':
				scenario.append(1)
			#check down
			if robby[0] == 9: #in bottom row, down is wall
				scenario.append(2)
			elif self.map[robby[0]+1][robby[1]] == 'O':
				scenario.append(0)
			elif self.map[robby[0]+1][robby[1]] == 'X':
				scenario.append(1)
			#check left
			if robby[1] == 0: #in left column, left is wall
				scenario.append(2)
			elif self.map[robby[0]][robby[1] - 1] == 'O':
				scenario.append(0)
			elif self.map[robby[0]][robby[1] - 1] == 'X':
				scenario.append(1)
			#check right
			if robby[1] == 9: #in right column, right is wall
				scenario.append(2)
			elif self.map[robby[0]][robby[1] + 1] == 'O':
				scenario.append(0)
			elif self.map[robby[0]][robby[1] + 1] == 'X':
				scenario.append(1)
			
			R = indiv.getResponse(scenario)
			if RMap[R] == 'random':
				R = random.randint(1,4)
			if RMap[R] == 'stay put':
				self.moves['moves'] += str(R)
			if RMap[R] == 'move Up':
				if robby[0] == 0:
					score -= 5
				else:
					robby[0] -= 1
			if RMap[R] == 'move Down':
				if robby[0] == 9:
					score -= 5
				else:
					robby[0] += 1
			if RMap[R] == 'move Left':
				if robby[1] == 0:
					score -= 5
				else:
					robby[1] -= 1
			if RMap[R] == 'move Right':
				if robby[1] == 9:
					score -= 5
				else:
					robby[1] += 1
			if RMap[R] == 'pickUp':
				if map[robby[0]][robby[1]] == 'X':
					score += 10
					map[robby[0]][robby[1]] = 'O'
				elif map[robby[0]][robby[1]] == 'O':
					score -= 1

d= Display()
#d.startThread()
d.mainloop()
	
