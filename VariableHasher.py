"""Hasher Class provides functions to hash a list of numeric variables into a single numeric value"""
class Hasher():
	def __init__(self, counts):
		"""counts is list of how many states each digit position has, LSB first"""
		self.counts = counts
	
	def hash(self,values):
		"""convert list of values into base ten index corresponding to that scenario"""
		h = 0
		if len(values) != len(self.counts):
			raise Exception, "number of values passed to hash is not equal to number of variables required"
		for i in range(len(values)):
			if values[i] > self.counts[i]-1:
				raise Exception, "Invalid value passed at index " + str(i)
			else:
				L=1
				if i == 0:
					L=1
				else:
					for j in range(i-1,-1,-1):
						L = L * self.counts[j]
				h += values[i] * L
		return h
	
	def unHash(self,value):
		"""Convert a base 10 value into a list of variable states that describe the same scenario"""
		x = []
		for k in range(1,len(self.counts)+1):
			L=1
			for i in range(len(self.counts)-k):
				L = L * self.counts[i]
			r = value / L
			value = value % L
			x.append(r)
		x.reverse()
		return x
	
	def countCombos(self):
		"""returns a 1 based value indicating the number of possible combinations from the given number of states per digit"""	
		x = 1
		for i in self.counts:
			x = x * i
		return x
