import Individual, random
import numpy as np

testCount = 25
moveCount = 100
mutation = 0.05
mapSize = 10
garbageRatio = 0.55
populationSize = 100
generationCount = 1000
keepBestEver = False

WALL = 2
GARBAGE= 1
EMPTY = 0

STAYPUT=0
MOVEUP=1
MOVEDOWN=2
MOVELEFT=3
MOVERIGHT=4
PICKUP=5
RANDOM=6

class Environment():
	#statusFile = open("status.txt","w")
	#codesFile = open("codes.txt","w")
	x = 0#Individual
	y = 0#individual
	individuals = []
	results = []
	
	def __init__(self):
		x = Individual.Individual(scenarioVars = [3,3,3,3,3], responses = [0,1,2,3,4,5,6])
		y = Individual.Individual(scenarioVars = [3,3,3,3,3], responses = [0,1,2,3,4,5,6])
		x.randomizeGeneticCode()
		y.randomizeGeneticCode()
		for i in range(populationSize):
			c = x.breed(y)
			c[0].randomizeGeneticCode()
			c[1].randomizeGeneticCode()
			self.individuals.append(c[0])
			self.individuals.append(c[1])

		
	def run(self):
		maxfit = -10000
		fittest = ''
		secondfit = -10000
		secondfittest = ''
		for generations in range(generationCount):
			if not keepBestEver:
				maxfit = -10000
				fittest = ''
				secondfit = -10000
				secondfittest = ''

			for indiv in self.individuals:
				fit = self.testFitness(indiv)
				if fit > maxfit:
					maxfit = fit
					fittest = indiv
				elif fit > secondfit:
					secondfit = fit
					secondfittest = indiv
			status = "gen: " + str(generations) + " fittest: " + str(maxfit) + " second: " + str(secondfit)
			result = {"gen":generations,
					  "bestScore": maxfit,
					  "bestcode":fittest.getGCode(),
					  "secondScore": secondfit,
					  "secondCode":secondfittest.getGCode()}
			self.results.append(result)

			print status
			#self.statusFile.write(status + "\n")
			#self.codesFile.write(status + "\n" + "fittest:\n" + fittest.getGCode() + "\n\n")
			#self.statusFile.flush()
			#self.codesFile.flush()

			self.individuals = []
			for i in range(populationSize):
				news = fittest.breed(secondfittest, mutation)
				self.individuals.append(news[0])
				self.individuals.append(news[1])
		#self.statusFile.close()
		#self.codesFile.close()
		
	def buildRandomMap(self):
		#declare the map as a matrix of zeros
		map = np.zeros((mapSize, mapSize),dtype=np.int)

		#set the boundaries to be walls
		map[0,:]= WALL 		#first row
		map[mapSize-1, :] =WALL #last row
		map[:, 0] =WALL 		#first column
		map[:, mapSize-1]=WALL #last column

		#loop over the interior region of the map and place some garbage
		for i in range(1, mapSize-1):
			for j in range(1, mapSize-1):
				if random.random() > garbageRatio:
					map[i,j]= GARBAGE
		return map

	def testFitness(self, indiv):
		"""This map tests the fitness of the given individual"""

		scores = []
		for tests in range(testCount):
			#build the map for this test
			map = self.buildRandomMap()

			#zero the score for this test
			score = 0

			#place robby somewhere in the interior of the map
			robbyX = random.randint(1,mapSize-2)
			robbyY = random.randint(1,mapSize-2)

			for moves in range(moveCount):
				#build scenario vars in order: center, up, down, left, right
				scenario = [map[robbyY, robbyX] ,  #center
							map[robbyY-1, robbyX], #up
							map[robbyY+1, robbyX], #down
							map[robbyY, robbyX-1], #left
							map[robbyY, robbyX+1], #right
							]

				R = indiv.getResponse(scenario)
				if R==RANDOM:
					R = random.randint(1,4)

				if R == STAYPUT:
					pass
				elif R==MOVEUP:
					if scenario[1] == WALL: #moved into wall
						score -= 5
					else:
						robbyY -= 1
				elif R == MOVEDOWN:
					if scenario[2] == WALL: #moved into wall
						score -= 5
					else:
						robbyY += 1
				elif R == MOVELEFT:
					if scenario[3] == WALL: #moved into wall
						score -= 5
					else:
						robbyX -= 1
				elif R == MOVERIGHT:
					if scenario[4] == WALL:
						score -= 5
					else:
						robbyX += 1
				elif R == PICKUP:
					if scenario[0] == GARBAGE:
						score += 10
						map[robbyY, robbyX] = EMPTY
					elif scenario[0] == EMPTY:
						score -= 1
				
					
			scores.append(score)	
		total = sum(scores)
		avg = total/float(len(scores))
		return avg

	def end(self):
		print "Ending"
		import datetime as dt
		now = dt.datetime.utcnow()
		nowStr = now.strftime("%m-%d-%Y_%H:%M")
		f = open("CodeDump_%s.pkl"%nowStr,'wb')
		import cPickle as cp
		params = {'testCount':testCount,
				  'moveCount':moveCount,
				  'mutation':mutation,
				  'mapSize':mapSize,
				  'garbageRatio':garbageRatio,
				  'populationSize':populationSize,
				  'generationCount':generationCount,
				  'keepBestEver':keepBestEver}
		res = {'params':params, 'results':self.results}
		cp.dump(res, f)
		f.close()


print "building"
try:
	e = Environment()
	print "running environment"
	e.run()
except:
	print "Error occurred, dumping results"
	e.end()


