import VariableHasher, random
import numpy

class Individual(object):
	hasher= VariableHasher.Hasher
	scenarioVarCounts = []
	responses = []
	name = ""
	def __init__(self, scenarioVars = [3,3,3,3,3], responses = [0,1,2,3,4,5]):
		self.scenarioVarCounts = scenarioVars
		self.responses = responses
		self.hasher= VariableHasher.Hasher(self.scenarioVarCounts)#Instansiate Hasher

	def breed(self, mate, mutationProbability=0.105):
		l = len(self._gCode_)#length of self genetic code
		if l != len(mate._gCode_):#check codes are same length
			raise Exception, "Cannot breed, mate has different length genetic code"
		split = random.randint(int(l/4.0),int(l/4.0)*3)#split the parent codes at some random point
		tick = 0
		newCode1 =numpy.concatenate((self._gCode_[0:split], mate._gCode_[split:]))
		newCode2 =numpy.concatenate((mate._gCode_[0:split], self._gCode_[split:]))

		mutations = 0

		for i in range(1,l-1):#loop through genomes
			if random.random() > (1- mutationProbability):#mutation at this genome?
				#apply random genome to each new code
				newCode1[i] = random.choice(self.responses)
				newCode2[i] = random.choice(self.responses)
			
		#spawn two new individuals using newcode1 and newcode2
		child1 = Individual()
		child2 = Individual()
		child1.GeneticCode = newCode1
		child2.GeneticCode = newCode2
		#print "mutations: " + str(mutations) + "  percent mutated: " + str(mutations/float(l)) + "  mutation probability: " + str(mutationProbability)
		return [child1,child2]
	
	_gCode_ = None#protected attribute to hold genetic code
	def getGCode(self):
		s = ""
		for i in self._gCode_:
			s+= str(i)
		return s
	def setGCode(self, code):
		L = self.hasher.countCombos()
		if len(code) != self.hasher.countCombos():
			raise Exception, "Genetic Code set to inappropriate length"

		if type(code) == str:
			self._gCode_=numpy.zeros((L))
			for i in range(L):
				self._gCode_[i] = int(code[i])	
		elif type(code) == numpy.ndarray:
			self._gCode_ = code

	GeneticCode = property(getGCode,setGCode)#genetic code as property tied to protected _gCode_
	
	def randomizeGeneticCode(self):
		"""Generate a completely random genetic code for this individual. Overwrites existing genetic code"""
		code = ""
		L = self.hasher.countCombos()
		for i in range(L):
			code = code + str(random.choice(self.responses))
		self.GeneticCode=code
	
	def setCodeTo(self, value):
		code = ""
		L = self.hasher.countCombos()
		for i in range(L):
			code = code + str(value)
		self.GeneticCode=code
	
	def getResponse(self, vars):
		"""Retrieve this individuals response to a particular scenario given by vars"""
		scenario = self.hasher.hash(vars)
		if scenario > self.hasher.countCombos():
			raise Exception, "Invalid Scenario"
		else:
			return self._gCode_[scenario]
	
	def __TEST__():	
		i1 = Individual()
		i1.setCodeTo(1)
		print "Code1:\n" + i1.GeneticCode
		i2 = Individual()
		i2.setCodeTo(2)
		print "\n\nCode2:\n" +i2.GeneticCode
		print "\n\nCode3 (bred):"
		i3 = i1.breed(i2)
		print i3.GeneticCode
