import Individual, random, emailer
from ftplib import FTP
individuals = []
x = Individual.Individual(scenarioVars = [3,3,3,3,3], responses = [0,1,2,3,4,5,6])
#x.GeneticCode = "254413355252212251453154554351452353350310255451454422455453255453150253154254011455240220452451440454111355450145453453441451354110041123450024102452141411514514153154354151155523154114012352352351152112424023032412311053142424233543241452004"
x.randomizeGeneticCode()
y = Individual.Individual(scenarioVars = [3,3,3,3,3], responses = [0,1,2,3,4,5,6])
#y.GeneticCode = "254413355252212251453154554351452353350310255451454422455453255453150253154254011455240220452451440454111355450145453453441451354110041123450024102452141411514514153154354151155523154114012352352351152112424023032412311053142424233543241452004"
y.randomizeGeneticCode()

for i in range(100):
	
	c = x.breed(y)
	individuals.append(c[0])
	individuals.append(c[1])



def testFitness(indiv):
	
	#Build Map 10x10
	
	
	
	
	
	#Randomly place robby
	
	scores = []
	for tests in range(25):
		map = []
		for i in range(10):
			map.append([])
			for j in range(10):
				if random.random() > 0.55:
					cell = 'X'
				else:
					cell = 'O'
				map[i].append(cell)
		score = 0
		robby = [random.randint(0,9), random.randint(0,9)]
		for moves in range(200):
			
			#build scenario vars in order: center, up, down, left, right
			scenario = []
			#check center
			if map[robby[0]][robby[1]] == 'O':
				scenario.append(0)
			elif map[robby[0]][robby[1]] == 'X':
				scenario.append(1)
			#check up
			if robby[0] == 0: #in top row, up is wall
				scenario.append(2)
			elif map[robby[0]-1][robby[1]] == 'O':
				scenario.append(0)
			elif map[robby[0]-1][robby[1]] == 'X':
				scenario.append(1)
			#check down
			if robby[0] == 9: #in bottom row, down is wall
				scenario.append(2)
			elif map[robby[0]+1][robby[1]] == 'O':
				scenario.append(0)
			elif map[robby[0]+1][robby[1]] == 'X':
				scenario.append(1)
			#check left
			if robby[1] == 0: #in left column, left is wall
				scenario.append(2)
			elif map[robby[0]][robby[1] - 1] == 'O':
				scenario.append(0)
			elif map[robby[0]][robby[1] - 1] == 'X':
				scenario.append(1)
			#check right
			if robby[1] == 9: #in right column, right is wall
				scenario.append(2)
			elif map[robby[0]][robby[1] + 1] == 'O':
				scenario.append(0)
			elif map[robby[0]][robby[1] + 1] == 'X':
				scenario.append(1)
			
			#Create Response list
			RMap = ['stay put','move Up','move Down','move Left','move Right','pickUp','random']
			R = indiv.getResponse(scenario)
			if RMap[R] == 'random':
				R = random.randint(1,4)
			if RMap[R] == 'stay put':
				pass
			if RMap[R] == 'move Up':
				if robby[0] == 0:
					score -= 5
				else:
					robby[0] -= 1
			if RMap[R] == 'move Down':
				if robby[0] == 9:
					score -= 5
				else:
					robby[0] += 1
			if RMap[R] == 'move Left':
				if robby[1] == 0:
					score -= 5
				else:
					robby[1] -= 1
			if RMap[R] == 'move Right':
				if robby[1] == 9:
					score -= 5
				else:
					robby[1] += 1
			if RMap[R] == 'pickUp':
				if map[robby[0]][robby[1]] == 'X':
					score += 10
					map[robby[0]][robby[1]] = 'O'
				elif map[robby[0]][robby[1]] == 'O':
					score -= 1
			
				
		scores.append(score)	
	total = 0
	for i in scores:
		total += i
	avg = total/float(len(scores))
	return avg
statusFile = open("status.txt","w")
codesFile = open("codes.txt","w")
e = emailer.emailer()

def postFTP():
	global statusFile
	statusFile.close()
	ftp = FTP('www.junkandstuff.net','u35899340-xianthrops','xianthrops')
	ftp.sendcmd('CWD /python/evo')
	file = open('status.txt','r')
	ftp.storbinary('STOR status.txt',file)
	file.close()
	ftp.quit()
	statusFile = open('status.txt','a')


for generations in range(1000):
	maxfit = -10000
	fittest = ''
	secondfit = -10000
	secondfittest = ''
	for indiv in individuals:
		fit = testFitness(indiv)
		if fit > maxfit:
			maxfit = fit
			fittest = indiv
		elif fit > secondfit:
			secondfit = fit
			secondfittest = indiv
	status = "gen: " + str(generations) + " fittest: " + str(maxfit) + " second: " + str(secondfit)
	print status
	statusFile.write(status + "\n")
	codesFile.write(status + "\n" + "fittest:\n" + fittest.getGCode() + "\n\n")
	statusFile.flush()
	codesFile.flush()
	if generations % 10 == 0:
		postFTP()
	individuals = []
	for i in range(100):
		news = fittest.breed(secondfittest, .03)
		individuals.append(news[0])
		individuals.append(news[1])
statusFile.close()
codesFile.close()
		

